# Quick Spell Casting

Cast spells with a single key press, similarly to how it works in Oblivion. Uses standard spell casting hotkey.

It is recommended to use the [use magic item animations](https://openmw.readthedocs.io/en/latest/reference/modding/settings/game.html#use-magic-item-animations) setting, because there is currently no way to discern between the spell and item cast time duration with the Lua API.

## Requirements

OpenMW 0.48